export function keyDown(key:KeyType){
    console.log(key)
}
export enum KeyType{
    /**上 */
    up,
    /**下 */
    down,
    /**左 */
    left,
    /**右 */
    right,
    /**A */
    A,
    /**B */
    B,
    /**选择 */
    select,
    /**开始 */
    start
}